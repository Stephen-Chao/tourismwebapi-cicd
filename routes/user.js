/*eslint no-console: "off" */
let User= require("../models/user")
let express = require("express")
let router = express.Router()
let mongoose = require("mongoose")
//let uriUtil = require("mongodb-uri")

let mongodbUri ="mongodb+srv://PengyuZhao:980628@attractions-5clzp.mongodb.net/Tourismsdb?retryWrites=true&w=majority"
mongoose.connect(mongodbUri,{useNewUrlParser:true})
mongoose.set("useFindAndModify",false)

let db = mongoose.connection
db.on("error", function (err) {
    console.log("Unable to Connect to User", err)
})

db.once("open", function () {
    console.log("Successfully Connected to User on atlas.com")
})

router.findOne = (req, res) => {

    res.setHeader("Content-Type", "application/json")

    User.find({ "username" : req.params.name },function(err, user) {
        if (err)
            res.json({ message: "User NOT Found!", errmsg : err } )
        else
            res.send(JSON.stringify(user,null,5))
    })
}
router.login = async(req, res) => {
    const {username, password} = req.body
    const dbUser = await User.findOne({username})
    if (dbUser) {
        dbUser.comparePassword(password, function (err, isMatch) {
            if (err)
                throw err
            if (isMatch) {
                res.send({
                    code: 1,
                    user: dbUser.toJSON(),
                    msg: "Successful to Login in"
                })
            } else {
                res.send({code: 0, msg: "Incorrect Password!"})
            }
        })
    } else {
        res.send({code: 0, msg: "No this user"})
    }
}
router.loginemail = async(req, res) => {
    const {email, password} = req.body
    const dbUser = await User.findOne({email})
    if (dbUser) {
        dbUser.comparePassword(password, function (err, isMatch) {
            if (err)
                throw err
            if (isMatch) {
                res.send({
                    code: 1,
                    email: dbUser.toJSON(),
                    msg: "Successful to Login in"
                })
            } else {
                res.send({code: 0, msg: "Incorrect Password!"})
            }
        })
    } else {
        res.send({code: 0, msg: "No this user"})
    }
}
router.Register = (req, res) => {
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    })

    user.save((err) => {
        if (err) {
            const error_type = err.errmsg.split("index: ")[1].split("_1 dup")[0]
            if (error_type === "email")
                res.send({"code": 0, "errorMsg": "Failed to register: email has been used!"})
            if (error_type === "username")
                res.send({"code": 0, "errorMsg": "Failed to register: username has been used!"})
        } else {
            res.send({"code": 1, "msg": "Successful to register!"})
        }
    })
}
module.exports = router
